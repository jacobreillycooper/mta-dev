﻿using System;
using Maths;
using System.Collections.Generic;
using Lists;

public class Program
{
    public static void Main()
    {
        List myListDemo = new List();
        string[] favouriteArtists = new string[] { "Miley Cyrus" }; // created an array with one item

        Stack<string> myFirstStack = new Stack<string>(favouriteArtists);

        myFirstStack.Push("Enimem");

        foreach (string item in myFirstStack)
        {
            Console.WriteLine(item);
        }

        myListDemo.listMaker();
        
    }
}