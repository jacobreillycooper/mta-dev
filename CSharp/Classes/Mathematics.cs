using System;

namespace Maths
{
    class Mathematics
    {
        public int addition(int numberOne, int numberTwo)
        {
            return numberOne + numberTwo;
            // addition is storing the value of the sum to be used over and over again
        }

        // give the method a access modifier? Yes. Public.
        // give the method a return type? int
        // give the method parameters? Yes. numberOne, numberTwo
        // why do we put int numberOne, int numberTwo? 
        // C# is a strongly typed language, which means every variable MUST have a type... even if its a parameter
    }
}