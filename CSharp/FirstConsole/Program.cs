﻿using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
        List<int> myFirstList = new List<int>();
		
		// Add to a list...
		myFirstList.Add(1);
		myFirstList.Add(3);
		myFirstList.Add(46);
		myFirstList.Add(78);
		
		for(int i = 0; i < myFirstList.Count; i++)
		{
			Console.WriteLine(myFirstList[i]);
			Console.WriteLine($"The value of i: {i}");
		}
    }
}