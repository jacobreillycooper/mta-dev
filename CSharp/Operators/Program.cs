﻿int hungerLevel = 100;

Console.WriteLine(hungerLevel - 10); // hungerLevel - 10 AND THEN moving on
Console.WriteLine(hungerLevel - 10); // hungerLevel - 10 AND THEN moving on
Console.WriteLine(hungerLevel - 10); // we aren't saving OR assigning the new sum value

// Console.WriteLine(hungerLevel = hungerLevel - 10); // assinging hungerLevel a new value
hungerLevel -= 10; // hungerLevel is equal to itself - 10
hungerLevel -= 10; // hungerLevel is equal to itself - 10
hungerLevel -= 10; // hungerLevel is equal to itself - 10
hungerLevel -= 10; // hungerLevel is equal to itself - 10
hungerLevel -= 10; // hungerLevel is equal to itself - 10
hungerLevel -= 10; // hungerLevel is equal to itself - 10

hungerLevel *= 10;
hungerLevel /= 10;

// the animal has gone for a run and is now a little bit hungrier
hungerLevel += 10;


Console.WriteLine(hungerLevel); // what is the value of hunger level?