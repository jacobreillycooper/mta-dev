using System;

namespace AnimalKingdom
{
    class Animal
    {
        public string animalName; // if we don't have the = sign then we need to give this variable a value when we create the instance
        // the only thing that changes when creating NEW variables is the type of data we want the variable to hold!
        public int age;
        public int hunger; // higher is hungrier
        public bool alive; // true is alive, false is.... ummm. not alive.
        public double favouriteNumber; // remember you don't need to put 'd' or 'f'

        // assign the animalName a value in Program.cs
        // assign the age a value in Program.cs

        public void birthday() 
        {
            age++;
        }
    }
}