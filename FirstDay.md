# Software Development

- Solution to a Software need
  - Website, Native Application, PWA (Progressive Web App), TV Applications, Mobile Applications, Console (Game, Terminal / Command Line)
- Many different programming Languages
  - C# (.NET), JavaScript, Python, Swift (iOS, Mac etc), Java, C, Ruby, PhP, and the list goes on

## Ways of writing code

1. Best Practice -> documentation and creators guide
2. Business practice -> what your business prefers... coding guide
3. Common practice -> your bedroom coder... it works... it's not good but it works? Try to stay away... Please?

## Our first Application

```csharp
using System;
// System is a library that we use in C# and .NET to make our code more efficient

// Class based application
// this means that we can access all of the .cs files in our folder easily

// All of our files MUST have a class
// That class MUST be capitalised
// In best practice, we name our class after the file name... which is also capitalised
public class Program
{
	public static void Main()
	{
        System.Console.WriteLine("Hiya, pal"); // if we don't include 'using System'
		Console.WriteLine("Hello, World");
	}
}
```

## Three naming conventions

- **Camel Casing**: lowercase letter for the first letter of the first word and then each new word's first letter is capitalised

* Alot of languages use camel casing except when naming Classes and Namespaces and other small items

```csharp
int myFirstInteger; // myFirstInteger
string name; // the first letter is lowercase
string myName; // the first letter is lowercase then the first letter of the second word is capitalised
string myFirstNameIsReallyShortButMySecondNameIsReallyLong;

// we use camel casing for everything apart from Classes and Namespaces in C#
```

- **Pascal Casing**: Each word's first letter is capitalised... even the first one...

```csharp

int MyFirstNumber; // each first letter of each word is capitalised
string MyFirstNameIsShortButMySecondNameIsReallyLong; // each first letter is capitalised

```

- **Snake casing**: no capitalisation... just underscores

```python
int my_first_number = 10
string my_first_name_is_short = 'Jacob'
# no capital letters... only underscores
# snake is not used in .NET / C#
```

# Variables

- Storage place for values we want to use in our application / program
- We have different variable types... these are called data types

## Data Types

- **Integer**: is a whole number... -2,147,483,648 to 2,147,483,647
- **float**: decimal number... can handle 6 to 7 decimal numbers
- **double**: decimal number... that handles double the amount that float does... 15 decimal numbers
- **boolean**: true or false value... think about on/off, yes/no
- **char**: stores a single character 'J' - single quotation mark ONLY
- **string**: string of text, characters... "Daniel"

### Integers

```csharp
int alHayaFavouriteNumber = 3;
int myFavouriteNumber = 4;

Console.WriteLine("Al-Haya's favourite number: 3");
Console.WriteLine("Al-Haya's favourite number: {0}", alHayaFavouriteNumber); // 3
alHayaFavouriteNumber = 5;
Console.WriteLine("Al-Haya's favourite number: {0}", alHayaFavouriteNumber); // 5
alHayaFavouriteNumber = 6;
Console.WriteLine("Al-Haya's favourite number: {0}", alHayaFavouriteNumber); // 6
```

### Float & Double

```csharp
float myFloatNumber = 14.564567f; //14.56F
double myDoubleNumber = 14.56; //14.56D OR 14.56d

// memory example
		float floatNumber = 1.200000f;
		double doubleNumber = 1.200000000000000;
```

### Boolean

```csharp
bool lightOn = true;
bool socksOn = false;

if(lightOn == true) {
    lightOn = false;
}
else {
    lightOn = true;
}
```

### Char

```csharp
char myCharacter = 'J';
```

### String

```csharp
string myName = "Jacob";
// double quotes or ``
```

## Variables & Constants

* Variables vary. The value can change, it doesn't mean it has to.
* Constants do not vary. They are constant.

### Constants
1. Date of Birth
2. Income Tax

* If you think there is any chance of something changing, allow it do so.

## Arrays

* Collection of items which we can access via an identifier... AKA index
* All arrays start counting at 0. 0 is the first number in our index
* Arrays cannot, and I mean cannot, change length once they have been created
* Arrays are immutable in length but not value, I can, however, change the value of an already defined value / index

```csharp
    // [] signal to the application that an array is being created
    //									0				1				2			3
    string[] favouriteArtists = { "Miley Cyrus", "James Blunt", "Taylor Swift", "Eminem" };
    //Console.WriteLine(favouriteArtists[0]);
    //Console.WriteLine(favouriteArtists[1]);
    //Console.WriteLine(favouriteArtists[2]);
    Console.WriteLine(favouriteArtists[3]); // eminem is the value
    // all variables can change... as soon as they are changed, 
    // from that point on, the value is not what it used to be
    Console.WriteLine("-------");
    favouriteArtists[3] = "Hugh Jackman but only in Greatest Showman"; 
    // enimem is deleted. Hugh Jackman reigns
    Console.WriteLine(favouriteArtists[3]);
    // favouriteArtists[4] = "Hannah Montana";
    // the array can hold 4 values and we asked it to hold 5
    
    string[] favouriteFilms = new string[3]; // created an array with a length of 3 but no values
    favouriteFilms[0] = "Air Force One";
    favouriteFilms[1] = "Happy Gilmore";
    favouriteFilms[2] = "Hannah Montana: The Movie";
    //Console.WriteLine(favouriteFilms[0]);
    //Console.WriteLine(favouriteFilms[1]);
    //Console.WriteLine(favouriteFilms[2]);
```

## Operators

### Comparison operators
		
		// < less than
		// > greater than
		// <= less than or equal to
		// >= greater than or equal to
		// == equal to
		
### Arithmitic operators

    * +, -, /, *

    * Modulus %
    ```csharp
    int numberOne = 10;
    int numberTwo = 3;

    Console.WriteLine(numberOne % numberTwo); // 10 % 3 = 1 // remainder of the sum = 1
    Console.WriteLine(numberOne % numberTwo); // 11 % 3 = 2 // remainder of the sum = 2
    Console.WriteLine(numberOne % numberTwo); // 12 % 3 = 0 // remainder of the sum = 0
    ```

    * Increment ++
    ```csharp
    int numberCheck = 10;

    for(int i = 0; i < numberCheck; i++)
	{
		Console.WriteLine(i);
	}
    ``` 
    
    * Decrement --
    ```csharp
    int numberCheck = 0;

    for(int i = 0; i > numberCheck; i--)
	{
		Console.WriteLine(i);
	}
    ```
		// + - / * % ++ --
		// += -= *= /= != 

## Conditionals

* Based on a condition, that condition being the value / paramter to check

```csharp
int numberOne = 4;
int numberTwo = 5;

if ( numberOne < numberTwo )
{
    Console.WriteLine("Number one is less than");
}
else {
    Console.WriteLine("It is either equal or greater than");
}

bool likedTheClass = true;
string instructorNameIfLiked = "Jacob";
string instructorNameIfNotLiked = "Daniel";

if (likedTheClass)
{
    Console.WriteLine(instructorNameIfLiked);
}
else 
{
    Console.WriteLine(instructorNameIfNotLiked);
}
```
### Long If Statement & it's replacement: Switch Case

* An if statement will check each condition, one after the other and this can be a lengthy process. Think about going shopping and going through the list one by one

```csharp
if(1)
{
    Console.WriteLine("Random text");
}
else if (2)
{
    Console.WriteLine("Random text");
}
else if (3)
{
    Console.WriteLine("Random text");
}
else if (4)
{
    Console.WriteLine("Random text");
}
else if (5)
{
    Console.WriteLine("Random text");
}
else if (6)
{
    Console.WriteLine("Random text");
}
else if (7)
{
    Console.WriteLine("Random text");
}
else 
{
    Console.WriteLine("HA.");
}
```

#### Switch Case

* Has conditions but the conditions check the answer and the correct one comes forward
* As said by Sarah: 'you don't go to the answer, the answer comes to you'

```csharp
string favouriteColour = "yellow";

switch(favouriteColour)
{
    case "purple": 
        Console.WriteLine("Purple");
        break;
    case "orange": 
        Console.WriteLine("Orange");
        break;
    case "blue": 
        Console.WriteLine("Blue");
        break;
    case "black": 
        Console.WriteLine("Black");
        break;
    default: // else statement in an if statement
        Console.WriteLine("I don't recognise that colour... check again.");
        break;
}
```


### Challenge

1. Please create an if statement that checks 3 ifs and an else... an integer can be created
2. Create a switch statement that checks a string for your favourite fruit... 3 cases and a default
3. Create an array that handles your favourite food but doesn't specific a specific length
4. Create an array that handles your favourite mode of transport and specifies a length


## Stacks

* **LIFO** (Last in, first out)
* A different way of storing a collection of data -> similar to an array in the sense that it is a collection
* Stack is inside of the System.Collections.Generic namespace... 
    * this means, to use it, we must call the System.Collections.Generic namespace
* Type specific... meaning, if we create an integer stack, we can only add integers

### What can we do inside of our Stack?

* **Count** the number of items in the stack
* **Push** items into the top of the stack (insert)
* **Peek** show the item at teh top of the stack
* **Pop** items from the top of the stack (remove)
* Check if the stack **Contains** a particular item... it has to match the full item name
* **Clear** all items from the stack

```csharp
using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
 
		string[] favouriteArtists = new string[] { "Miley Cyrus" }; // created an array with one item

		Stack<string> myFirstStack = new Stack<string>(favouriteArtists);
		
		myFirstStack.Push("Enimem");
		myFirstStack.Push("50 Cent");
		myFirstStack.Push("Hannah Montana");
		
		// myFirstStack.Pop(); // what is this going to do?
		// myFirstStack.Pop();
		
		string topOfTheStack = myFirstStack.Peek(); // what is the result of this?
		
		Console.WriteLine(topOfTheStack);
		
		Console.WriteLine(myFirstStack.Contains("50"));
		
		//myFirstStack.Clear();
		Console.WriteLine("----");
		
		foreach (string item in myFirstStack)
		{
			Console.WriteLine(item);
		}                   
	}
}
```

## Queues

* **FIFO** First in, first out
* Collection of items, similar to a Stack
* It exists inside of System.Collections.Generic and can only be used if that is called

### What can we do inside of our Queue?

* **Count** the number of items in the Queue
* Add items to our Queue using **Enqueue**
* Remove items from our queue using **Dequeue**... from the front
* **Peek** the first item... it doesn't remove it
* Check if the Queue **Contains** a particular item... it has to match the full item name
* **Clear** all items from the Queue

```csharp
using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
		Queue<int> myFirstQueue = new Queue<int>();

		myFirstQueue.Enqueue(123);
		myFirstQueue.Enqueue(1);
		myFirstQueue.Enqueue(4);
		myFirstQueue.Enqueue(5);
		myFirstQueue.Dequeue();
		myFirstQueue.Enqueue(16);
		myFirstQueue.Peek(); // what will the answer be at this point?
		myFirstQueue.Contains(7); // what will the answer be? true or false
		myFirstQueue.Enqueue(12345);
		
		Console.WriteLine(myFirstQueue.Count);
		
		foreach(int number in myFirstQueue)
		{
			Console.WriteLine(number);	
		}           
	}
}
```

## Challenges for Queues and Stacks

3 things:
* Peek value
* Contains value
* End result of the queue

1. Queue challenge:
```csharp
Queue<int> myFirstQueue = new Queue<int>();

Enqueue(123);
Enqueue(1);
Enqueue(4);
Enqueue(5);
Dequeue();
Enqueue(16);
Peek(); // what will the answer be at this point?
Contains(7); // what will the answer be? true or false
Enqueue(12345);
// What is the final result?
```

## Lists

* Strongly typed collection of items
    * We define the type of data we want... int, string etc
* Exists inside of the System.Collections.Generic
* Contain only one type... 

### What can we do inside of a List?

#### Properties

* Don't require the () syntax at the end
* **Items** can be checked with the property (rarely used in Lists)
* **Count** the number of items in the List

#### Methods

* Do requie the () syntax at the end
* **Add** a single value to the list at the end using Add()
* **AddRange** to the list... a range is a collection of items... this goes at the end too
* **Insert** a single value at a specific index point inside of the list
* **InsertRange** to the list at a specific index point... a collection of items
* **Remove** an item at the first occurance of the element
* **RemoveAt** - removes an item at a specified index point
* **RemoveRange** Removes a collection of items that match the collection
* **Sort** all of the items into order
* **Clear** all items

```csharp
using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
 		List<int> myFirstList = new List<int>();
		
		// Add to a list...
		myFirstList.Add(1);
		myFirstList.Add(3);
		myFirstList.Add(46);
		myFirstList.Add(78);
		
		// Creating an array for our AddRange() method
		int[] myIntArray = new int[3]{4, 6, 8};
		
		// Adding the array to our List via the AddRange() method
		myFirstList.AddRange(myIntArray);
		
		// Insert a value at a specific index value
		myFirstList.Insert(3, 15);
		
		// Inserting a range: a collection of items at a specific point
		myFirstList.InsertRange(0, myIntArray);
		
		// Remove - this will remove the first instance of the value / item you give it
		myFirstList.Remove(4); // one of the 4s remains...
		
		//RemoveAt - this will remove an element at the specified index point
		myFirstList.RemoveAt(3); // the third index value is removed from the list
		
		// Remove items from the first index and remove 5 items
		myFirstList.RemoveRange(1, 1); 
		
		// Checks if an item exists
		Console.WriteLine(myFirstList.Contains(4));
		
		// Sorting the list
		myFirstList.Sort();
		
		for(int i = 0; i < myFirstList.Count; i++)
		{
			Console.WriteLine("The value of i: {0}", i);
			Console.WriteLine(myFirstList[i]);
		}
	}	
}
```

## Learn On Demand

https://firebrand-uk.learnondemand.net/Class/444977

- Your email address
- Pa$$w0rd

# C# on the browser

https://dotnetfiddle.net/

# Documentation Links

* https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/
