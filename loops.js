// Loops in JavaScript

// for loops, for/of, while and do/while

let favouriteSongs = ["Song 1", "Song 2", "Song 3", "Song 4", "Song 5", "Song 6"]

for (let i = 0; i < favouriteSongs.length; i++)
{
    console.log(i)
    console.log(favouriteSongs[i])
    // what is it doing?
    // looping through the favouriteSongs array until there is nothing left
}

// for loops need three things:
    // initialisation: let i = 0; creating the variable so nothing else conflicts
    // condition: i < the length of favouriteSongs array
    // iteration: increment or decrement

let people = ["Miley Cyrus", "Taylor Swift", "Enimem"]

for (let item of people)
{
    console.log(item)
    // for each item inside of the people array, console.log that item
}

// for of loops came in in 2016, older browsers don't support it: internet explorer

let number = 10 

while (number < 20) { // is number less than 20? yes.
    console.log(number) // print 10
    number++
    // how many times?
}

// a while loop is probably the simplest loop but also the most dangerous
// a while loop needs two things:
    // a condition - number < 20
    // a value check / change - number++
    // if we don't do this, we will get stuck in an infinite loop

let newNumber = 100

do {
    console.log(newNumber) // will this print?
    newNumber++
} while (newNumber < 10);

// do while... executes AT LEAST once
// even if the condition is false... it is guarenteed to run at least once... maybe more?

// TODO: Please create 2 of each loops: 16:50 come back and discuss
// for loop, for of loop, while and do while loop
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration