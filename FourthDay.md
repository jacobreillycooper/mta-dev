# Fourth and final day

* JavaScript functions
* JavaScript and how it affects web performance
* SQL -> databases, tables, records, data, data, data

## JavaScript Data types

* string
* number -> is both integer and decimal
* boolean
* null

## JavaScript functions

* Methods exist in JavaScript too... they exist inside of a class
* Outside of a class, they are functions

* **Method and Function example in JS**
```js
class Person {
    constructor(name, age) {
        this.name = name,
        this.age = age
    }
    // method
    birthday() {
        this.age++
    }
}

let age = 10;

// function
const birthday = () => {
    age++
}

birthday()
```

* Functions can be created in a couple of ways in JavaScript
    * **function** keyword -> before 2016, this was the way to do it and you can still do it this way... you just look old.
    ```js
    let age = 20;
    // function
    function birthday(age) {
        console.log(age); // this will log the age
        age++; // this will increase age by one each time it is CALLED
        console.log(age); // this will log the age
    }
    birthday(age); // this will run and show 20 && 21 in the console
    birthday(15); // this will run and show undefined and NaN in the console

    ```

    * **const** keyword can be used to create a function after 2015/6 (ES6)
    ```js
    let age = 20;
    // arrow syntax function
    const birthday = () => {
        // () still take a parameter / argument (age)
        // birthday EQUALS the following code
        age++; // this will increment age
    }

    birthday(); // this will make it 21
    birthday(); // this will make it 22
    birthday(); // this will make it 23
    ```

    * Why do we use functions?
        * Reusable code that we don't have to repeat
        * Perform a task, multiple times with one line of code

## var vs let

* Very similar in most cases, they don't cause any problems at all when interchanged 
* var has a problem though with loops
* Take a look at [hoisting](https://www.digitalocean.com/community/tutorials/understanding-variables-scope-hoisting-in-javascript)

```js
for (var i = 0; i < 10; i++)
{
    console.log(i);
}

console.log(i); // this will show the answer 10... i should be deleted after the loop is complete but var doesn't delete it. It creates it globally

// comment the above code out and then run the below 
for (let i = 0; i < 10; i++)
{
    console.log(i)
}

console.log(i); // Uncaught ReferenceError: i is not defined at variables.js
// let i = 0 is only created for the for loop and then it is forgotten

```

## Scoping

* We have different scopes for our variables
* We have different ways to access variables we create

1. Global Scope
    * Global can be accessed anywhere in the file... anywhere BY anything in the file!
    * The problem with Global is that it can be accessed anwhere by anything in the file
2. Local Scope
    * Local cannot be access anywhere... it can be accessed in the area that it is created
3. Block / Function scope