-- CREATE DATABASE employees;
-- once we have created the database, we must USE the database

-- USE employees;

-- DROP DATABASE employees;

CREATE TABLE employee_information (
	 EmployeeID int PRIMARY KEY,
	 FirstName varchar(255),
	 -- varchar will hold letters / characters and up to 255 in length
	 LastName varchar(255),
	 Location varchar(255),
);

-- DROP TABLE employee_information;

-- A table should have a primary key... unique identifier
-- something that will not be duplicated
-- employeeID
