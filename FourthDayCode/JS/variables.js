var firstName = "Jacob"
let lastName = "Reilly-Cooper"

console.log(`${firstName} ${lastName}`)

for (let i = 0; i < 10; i++)
{
    console.log(i);
}

// for (i = 0; i < 100; i++) // will declare i as a var not a let
// {
//     console.log(i);
// }


// for (let i = 0; i < 10; i++)
// {
//     console.log(i)
// }

