let globalVariable = "true";

function scopeCheck() {
    let localVariable = "jacob";
    console.log(globalVariable) // this is a global variable call
    console.log(localVariable);
    
    function anotherFunction() {
        console.log(localVariable); // localVariable can be found when you go back one indent
        let nestedLocalVariable = true;
    }
    
    // console.log(nestedLocalVariable) // will I be able to go inside of anotherFunction() 
    //! No. You cannot go into another function to check if a variable has been created if you didn't find it on your level or back one level
}

function birthday() {
    let age = 0;
    age++;
    // console.log(localVariable) // can I call / access localVariable here? 
    //! No. I cannot go inside another function to find the variable
}

function dilemma() {
    var varVariable = true;
    console.log(varVariable);
}

// console.log(varVariable); // varVariable can be accessed here?
//! No.

dilemma()

console.log(varVariable);

// birthday()

// scopeCheck();