let globalVariable = true;

console.log(globalVariable) // this is a global variable call

function scopeCheck() {
    let localVariable = "jacob";
    console.log(globalVariable) // this is a global variable call
    console.log(localVariable);
}

let firstName = "Sarah"; // this can be modified
for (let i = 0; i < 3; i++) {
    firstName = "Bob" // see? Sarah is now called Bob
    console.log(firstName);
}

scopeCheck();

// localVariable
console.log(firstName)
console.log(localVariable);
// console.log(anotherLocal);
// abc means that word has been written but I can't access it
