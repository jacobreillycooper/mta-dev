
// // the reason they were logged is because we passed 20 as a value with the variable age... 
// // it's a nice educational tool to pass age on line 8


// let value = 23.5; // number
// value += "123"; // this will be 23.5123
// now this gets a little confusing BUT don't worry... it always will be



// let value = 23.5 + "jacob"; // error or naaah?
// console.log(typeof value); // type string
// console.log(`Value is ${value}`);

function birthday(age) {
    console.log(age); // this will log the age
    console.log(typeof age);
    
    age++; // you want me to add 1 to a string?
    // is age, the string, a valid number? If so, I can implicity convert to a number
    // if it's not... I won't error, I will just say NaN
    
    console.log(age); // this will log the age
    console.log(typeof age);
}

let age = 0; //

birthday(age); 
birthday(age); 
birthday(age); // what will we get here? 

// why could true be a number?
// true / false, on / off, 1 / 0

// 0, 1
// 1, 2
// 2, 3