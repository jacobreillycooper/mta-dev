// function birthday(age) { // age is = 0 before we run the function
function birthday() { // age is whatever the value of age is at the point the function is run
    // console.log(age); // this will log the age
    age++; 
}

let age = 0; 

birthday(age); 
console.log(age); // 1
birthday(age); 
console.log(age); // 2
birthday(age);
console.log(age); // 3

age = 100;
birthday(age); // 100 + 1
console.log(age); // 101

// check line 1! Each time, we give it the value 0...
// check line 2! Each time, we just ask for the age to be incremented