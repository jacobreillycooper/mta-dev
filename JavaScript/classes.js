

// object is a variable that holds key : value pairs
// key: firstName, value: "Pavel"
// key: age, value: 37




const avni = {
    firstName: "Avni",
    age: 18,
    location: "London",
    job: "Junior Developer",
    car: true,
}

avni.legs = true

const abdi = {
    firstName: "Abdi",
    age: 18,
    location: "London",
    job: "Junior Developer",
    car: true,
    legs: 22
}

// that's not efficient
// we use classes
// constructor is an important part of our class
// the constructor looks for you to assign the value

class Person {
    constructor(firstName, age, car, legs) {
        this.firstName = firstName
        this.age = age
        this.location = "London"
        this.job = "Junior Developer" // job is always going to be Junior Developer unless you change it later
        this.car = car
        this.legs = legs
    }

    birthday() {
        this.age++
    }
}


//                      name, age, car, legs
const adam = new Person("Adam", 27, true, "asdfghjkl")
const pavel = new Person("Pavel", 37, true, true)
const sarah = new Person("Sarah", 22, true, true)
const roxana = new Person("Roxana", 22, true, true)
console.log(adam)
console.log(sarah)
console.log(pavel)
roxana.location = "Near Liverpool"
console.log(roxana)
pavel.birthday()
console.log(pavel.age)
