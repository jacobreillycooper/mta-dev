console.log("Hello, World")

let myArray = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"]

// console.log(myArray)

myArray.push("Item 6") // push adds an item / element to the back of the array

myArray.pop() // removes the last item from the array
myArray.pop() // removes the last item from the array
myArray.pop() // removes the last item from the array
myArray.pop() // removes the last item from the array
myArray.pop() // removes the last item from the array
myArray.pop() // removes the last item from the array
myArray.pop() // removes the last item from the array
// myArray.shift() // removes the first item from the array

// myArray.splice(0, 2)

myArray.push("Item 6") // push adds an item / element to the back of the array
console.table(myArray)