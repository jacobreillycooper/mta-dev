// camelCasing unless I say otherwise

// JS allows you to be flexible with your variables
// for strings, you can use '' or "" but be consistent. If you use '' then make them ''... if you use "" then make them all ""

// let firstName = "Jacob" // JS knows that this is a string
// let lastName = "Reilly-Cooper"

// let myAge = 56

// let lightOn = true

// let nothing = null // is empty


// var myName = `Jacob`

// const myFaveNumber = 4

// // there are different ways to put variables into strings

// // option 1
// console.log("My first name is " + firstName + " and I am " + myAge + " years old. The light in my house, is on.. that is " + lightOn + ".")

// // option 2
// console.log(`My first name is ${firstName} and I am ${myAge} years old. The light in my house, is on.. that is ${lightOn}.`)

// // arrays

// let myArray = ["Hello", "Goodbye", "Whateva"]

// console.log(myArray[0])
// console.log(myArray[1])
// console.log(myArray[2])

// console.table(myArray)

// myArray.push("How'ya'doin")
// myArray.pop()

// myArray.shift()
// myArray.shift()
// console.table(myArray)

// let animals = ["cat", "Chicken", "dog", "Pig", "Raccoon"]
// let returnValue = animals.filter(
//     animal => animal.includes("c")
// )
// console.log(returnValue) // which animals will display in the console log?



// // TODO: Please create two arrays with any context that holds 5 items to start THEN remove the last item, add 2 more and then remove the first

// let favouriteSongs = ["Song 1", "Song 2", "Song 3", "Song 4", "Song 5"]
// // favouriteSongs.pop() // removing the last item
// favouriteSongs.push("Song 6", "Song 7") // adding 2 more
// // favouriteSongs.shift() // removing the first item

// console.table(favouriteSongs)

// favouriteSongs.splice(1, 3, "Song 8", "Song 9")
// favouriteSongs.sort()
// favouriteSongs.reverse()

// console.table(favouriteSongs)

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array

// Loops