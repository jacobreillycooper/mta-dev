# SQL

* Structured Query Language
* Database development

* It is used in 70% of all projects that hae a database connection

## What can hold a database or need a database?

* Data :-)
* Student Information
* Ecommerce websites
* Anything that needs to hold a record of any kind

* If you want to access that record on the application and change it, you need to store it somewhere... that somewhere is a database! (DB)

**EXCEL SPREADSHEETS ARE NOT DATABASES**

## Different flavours of SQL

* SQL
* MS-SQL
* MySQL
* MySQL Workbench
* **PostgreSQL**
* SQLite
* Mini-SQL
* Marie
* RDS - Amazon's Relational Database service
* Azure SQL
* MS Access SQL
* Oracle SQL
* Cloud SQL

* That looks like a lot to learn? Don't worry! They all use the same syntax bar a few differences!! But you will learn those with a particular stack (language / framework)

## Different ways to run SQL 

* SQL Server Management Studio
* Azure Data Studio (this looks exactly like VSCode)
* MySQL Workbench
* IntelliJ IDE
* SQLGate


## SQL METHODS

* CREATE -> create databases & tables
* INSERT -> into a table the information
* UPDATE:
    * Please be careful with update!!!!!!!!!!!!!!!!!!!!!
    * You must use the following keywords when updating a record:
        * UPDATE, SET && WHERE
        * If you don't use WHERE, it will update all records in your table... all of them!
* DELETE
    * You must use the following keywords when DELETING a record:
        * DELETE && WHERE
        * If you don't use WHERE, it will DELETE all records in your table... all of them!
* SELECT
    * SELECT * FROM table_name; -- the * is a wildcard option
    * SELECT Location FROM table_name; 

**CRUD** CREATE, READ, UPDATE & DELETE