# Day 3

## What are we looking at?

* Application Lifecycle Management
* HTML, CSS & **JavaScript**
* Recapping anything you'd like from last week

## Application Lifecycle Management

* SDLC (Software Development Lifecycle), STLC (Software Testing Lifecycle), Application Lifecycle, Lifecycle, that thing we use to develop apps / software

* Set of sections / activities that focus on creating THE *perfect* product

* Waterfall Model, **Agile**(ish), SCRUM, RAD, Sprints, Spirals

## Job Roles in a Tech Business

* Developer
    * Lead
    * Senior
    * Mid
    * Junior / Apprentice
* Designer
* Technical Architects
* Ops team
* Customer Service
* Business Analysts (BAs)
* Project Manager
* Delivery Manager
* Content Writers
* Marketing 
* Sales / Finance
* Tester
    * Different strands of tester

### Waterfall

* The classic. The catalyst of all of the other models.
* It's a bit... outdated. 
* You *must* complete each stage before moving on to the next one... if you run into a problem, you *have* to go back to the start.

## The stages

* Feasibility study *(Only in some lifecycles)*
* Requirements
* Design
* Development
* Testing
* Maintanance / Deployment
* Deployment *(Only in some lifecycles)*

### Requirements

* Who is responsible?
    * Business Analysts (BAs)
    * Project Manager
    * Developers
    * Designers
    * TAs

* Requirements of the application
    * It's intended use
        * The use case for the application -> the genre of application: streaming service, game, utility etc
        * What **purpose** does the application serve?
        * Is there anything else out there like it?
    * Application Specifcation
        * What it has to be? A tick box of requirements
        * The scope of the specification
            * The cost
            * The timeline -> how long have we got and what needs doing in the sections of the timeline:
                * A year long application may be split up into:
                    * 3 month chunks... 4 sections with aims for each section
                    * Twelve 1 month chunks... little and often approach
                    * We need to split our time up properly
                * Your timeline split up depends on many things:
                    * Resources - staff, equipment, buildings etc
                    * Cost
                    * Skills -> is training needed? Not all projects will fit your employees skills!!!!
                    * Technology
                    * Outside developments
                    * Loads more
                * Contigency time
            * Where it can gooooo? Incredibly exciting. Terrifying.
            * Scope creep -> what on earth is this?
    * System resources
        * What does the application need to run?
        * What does the user need?
        * Minimum Viable Product (MVP) - what is the minimum we can produce to provide a successful application?
    * Cost 
        * Employees - salary + benefits and pension
        * Technology
        * Maintanance -> cloud, repairs, improvements
* Requirements of the staff
    * Training
    * What will the staff need to be working on during the **development** of the application
* Requirements of the user
    * Minimum specification for the user?
    * What do they need to know to be able to use the software?
        * Are we phasing out an old piece of software?
        * Is this brand new?
        * Is training provided?
* Requirements of the business (both of them in some cases)
    * What does the business need to have in place to run the software?

* A list of requirements -> bullet point checklist of what is needed and NOT needed

### Design

* Graphical Design -> what it looks like!! GUI - Graphical User Interface / CLI - Command-Line Interface
* Structural / architecture design
    * Cloud, On-premise, SaaS, PaaS
    * What languages are we going to use? What frameworks?
    * How will the languages connect?
        * How will JavaScript speak to the database?
        * Which DB are you going use?
        * How will the user be able to report problems etc?
* Ideas for the app

* Who do you think approves this process?
    * Designers
    * TAs

### Development

* Start Coding and hopefully finish
* Develop ideas from the design stage / Implement the ideas
* Connection of Databases
    * Relational / SQL
    * Non-Relational / No-SQL
* Think about the UX
* Developing documentation for the company, for the other developers and for the users

* Functional reqs

### Testing

* Assuring that your product / application / software is of an acceptable quality
* What are the gaps between requirements and reality?
* Testers  / Developers

* Verify the software against the requirements
* Find as many defects as we can, but we cannot and should not gauatee the absence of defects

* Testing early will result in a cheaper application cost

* TDD - Test-Driven development
    * Red and green cycle - fail and pass cycle
    * Write your test and then you write the code for the module / function you need
    * Write more focused tests for our app

* White-box
    * A developer or tester has an in-depth knowledge of a system that they are testing
    * Test each method / function and ensure that they have appropriate test cases
    * This is length process compared to black-box
* Black-box
    * Only worry about the input and the output... not how it from one to the other
    * Does the software meet the requirements from the user input to the user output?
    * You don't need full extensive knowledge of the inner-workings of the app
* Unit Testing
    * What do we mean by a unit?
        * Variable, function / method, array, loop, file, aaaaabsolutely anything that works in the app
        * Does that **thing** work?
        * It means that your testing can be quite length!
* Integration
    * How well does a piece of software integrate with the rest of the app?
    * A, B, C
        * Check that A works with B
        * Check that A works with C
        * Check that A works with B && C
        * Check that B works with C
* System
    * How well does the system work?
    * Look at the overview... does it achieve what it was meant to?
    * Where are the falldowns?
* Regression
    * Making that sure that new fixes don't cause more problems
    * You have to have to a list of broken features...
    * A list of things work
    * Has something you've changed just broken the entire project
* Does it work?

**Manual Testers**

* Go through an application and test everything by hand
* Lengthy process but a personal process
* Expensive because of the length and the extreme you have to go to test

**Automation Testers**

* Quicker process than manual in MOST cases
* Should be less expensive
* You CAN test more but that doesn't always mean that you do

### Maintanance (sometimes with Deployment)

* How are we, as a company, going to look after the software?
* How long are we, as a company, going to look after the software?
* Who is going to look after the software?

* Are we going to add new features in the future and if so, how? And who?
* Ensuring it remains usable!!!