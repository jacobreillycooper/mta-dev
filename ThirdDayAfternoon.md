# JavaScript

* Web Development Programming Language that adds features and complexity to a website

## Things about JavaScript

* The main files of a project are normally either:
    * index.js OR main.js

* People used to name their homepage: homepage.html
    * All homepages should be index.html or index.js
        * Domain / Service provider / Hosting Package searches for index first... if you have the following structure:
            * home.html
            * about.html
            * contact.html
        The Hosting package will run about.html first

```js
// camelCasing unless I say otherwise

// JS allows you to be flexible with your variables
// for strings, you can use '' or "" but be consistent. If you use '' then make them ''... if you use "" then make them all ""

let firstName = "Jacob"
let lastName = "Reilly-Cooper"

let myAge = 56

let lightOn = true

let nothing = null
```

* Three ways of defining variables:
    * 1. 
    ```js
    let myName = "Jacob" 
    ```
    * 2.
    ```js
    var myName = "Jacob";
    ```
    * 3.
    ```js
    const myName = "Jacob";
    ```

## HTML

* HTML doesn't compile, it doesn't check for errors, it doesn't really do anything... it just displays the text on the page

* Top-down writing structure
* Each page has a <html>, <head>, <body>
* Every thing inside of **<>** are called tags
    * <body> is the body tag
    * <h1> is the h1 tag
    * <footer> is the footer tag and so on
* The majority of tags need an opening and a closing tag
    * <body> </body> an opening and a closing tag
    * <footer> </footer> an opening and a closing tag
* Some of the tags don't have a closing tag
    * <img />

* The <head> contains all the information we don't want the user to worry about
* The <body> is the show-off content -> the stuff you're proud of. The sexy stuff.
* The <footer> is the legal information, the sitemap etc. How often do you look at someone's shoes?

* The <html> surrounds everything - you put all of your content, your head, body, footer into the <html>

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
</body>
</html>
```