# Object-Oriented Programming
 
* Understand the features of a class and know difference between a class and an object
* Be familiar with the three main concepts of object-oriented design, along with the C# syntax used to implement them

## Creating our first project

1. Create a folder called **Classes** in Documents / File Explore (or a folder of your choice)
2. Open **visual studio code** -> click Open folder and click on the folder **Classes**
3. Click on **Terminal** at the top of the window and click 'New Terminal'
4. Type in **dotnet new console** and press enter
5. Type in **dotnet run** and press enter
6. This should run "Hello, World!"

## Three programming Paradigms
* OOP
* Procedural - Line by line
* Event-driven - based on events performed by the user / program

## What is OOP? (Object-Oriented Programming)

* OOP is made up of **Classes** and **Objects**
* Clear and clean structure for our applications / programs
* Reusable code -> DRY: Don't repeat yourself
* Reusable code -> DRY: Don't repeat yourself

## Namespace

* Namespace is a storage place for our classes
* Example would be:
    * **AnimalKingdom** will hold all of our Animals... Lion, Cow, Pig, Sheep, Bird
    * **MotorVehicles** will hold all of our Motor Vehicles... cars, trucks, artics, motorbike, reliant robins
    * **Firebrand** will hold all of the necessary classes for Firebrand... Employees, Students, Contractors

## Public static void Main()

* public -> access modifier... public, private, protected...
    * public allows the property that we are making public to be accessed anywhere
* static -> We call the method without an object
* void -> the method returns no value!! See below for further notes!
* Main() -> this is our Main method: the entry point of our program... all things in the program will get accessed by this method in one form or another... directly OR indirectly


## Access Levels

* public -> accessible to any file in the program, to be modified at will
* private -> accessible to the class that the item has been created and only that class

### Classes & Objects

* Classes are templates for our objects
* Objects are instanes of a class... Object is a version / copy of our class
* Class is a like a recipe for our object

* In our classes, we will handle two things:
    1. **Variable** to handle attributes like name, hungerLevel, tiredness, age etc
    2. **Methods** to modify our attributes however we like:
        * We can create a *sleep* method to lower our *tirednessLevel* attribute
        ```csharp
        public int tirednessLevel = 100;
        public void sleep() {
            tirednessLevel -= 10; // 100 to 90
        }
        ```

* In our methods, it is best to use the following operators:
    * +=, -=,/= *= instead of +, -, / *

    ```
    hungerLevel = hungerLevel - 10;
    hungerLevel -= 10;

    // hungerLevel is equal to itself - 10

    ```

### Example of Classes and Objects

* Program.cs
```csharp
using System;
using AnimalKingdom; // this saves us typing out AnimalKingdom.Animal or AnimalKingdom.Lion

public class Program
{
    public static void Main()
    {
        Animal pavel = new Animal(); // instance of the Animal class...
        Animal bob = new Animal(); // instance of the Animal class...
        Animal dave = new Animal(); // dave will be a new copy / instance of Animal...
        // all of the above instances will take the variables and the methods from the Animal class

        // we can create many Animals...
        // an instance is an object... so, pavel is an object... bob is an object... dave is an object...
        pavel.animalName = "Pavel";
        Console.WriteLine($"My name is: {pavel.animalName}");
        //    Console.WriteLine($"My name is: {bob.animalName}");

        pavel.hungerLevel = 100;
        pavel.tirednessLevel = 100;

        pavel.eat(); // this will lower the hunger level by 10
        pavel.sleep(); // this will lower the tiredness by 10 and raise the hunger level by 5

        // 100 - 10 = 90... we then raise it by 5... 95

        Console.WriteLine($"My hunger level is: {pavel.hungerLevel}"); // what will it be here?
        // 85, 95

    }
}
```
* Animal.cs
```csharp
using System;

namespace AnimalKingdom
{
    class Animal 
    {
        public string animalName; // letting the user choose the name
        public int hungerLevel; // higher is more hungry (1-100)
        public int tirednessLevel;
        public void eat() {
            hungerLevel -= 10;
            // hungerLevel -= 10;
        }
        public void sleep() {
            tirednessLevel -= 10;
            hungerLevel += 5;
        }
    }
}
```

* **Animal** is our class -> our template for our animal will have the following attributes
    * Brain
    * Movement
    * Breathe
    * Hunger
    * Strength (Moderate)
    * Sleep / Tiredness

## Challenge for Animal.cs project

* Create 4 variables in our class Animal.cs
* Create 1 method, calling it what you like
    ```csharp
    public void eat() {
        hungerLevel -= 10;
    }
    ```
* In Program.cs, give the 4 variables values and Console.WriteLine those values
* In Program.cs, call / use the 1 method and write down expected results
    * E.g. // Hunger Level to go from 100 to 90 when logged

### Answer to the challenge
* Program.cs
```
using System;
using AnimalKingdom; // step 1... import the AnimalKingdom namespace using the 'using' keyword

public class Program
{
    public static void Main()
    {
        Animal dave = new Animal(); // step 2... create the instance...
        Console.WriteLine(dave);

        // what can dave do? how do we check what dave can do?
        dave.animalName = "Dave";
        Console.WriteLine(dave.animalName);

        dave.age = 1;
        dave.birthday();
        dave.birthday();
        dave.hunger = 100; // dave is very hungry
        dave.alive = true; // yay.
        dave.favouriteNumber = 4;
        Console.WriteLine(dave.age);


        // Console.WriteLine(dave.hunger);
        // Console.WriteLine(dave.alive);
        // Console.WriteLine(dave.favouriteNumber);
    }
}
```
* Animal.cs
```csharp
using System;

namespace AnimalKingdom
{
    class Animal
    {
        public string animalName; // if we don't have the = sign then we need to give this variable a value when we create the instance
        // the only thing that changes when creating NEW variables is the type of data we want the variable to hold!
        public int age;
        public int hunger; // higher is hungrier
        public bool alive; // true is alive, false is.... ummm. not alive.
        public double favouriteNumber; // remember you don't need to put 'd' or 'f'

        // assign the animalName a value in Program.cs
        // assign the age a value in Program.cs

        public void birthday() 
        {
            age++;
        }
    }
}
```


## Erorr Messages

### Missing Semi-Colon
* Program.cs(2,20): **error CS1002**: ; expected
    * Program.cs is my file where the error is...
    * (2,20) -> line 2, column / space 20...
    * CS1002: ; expected

### Missing Bracket
* Program.cs(4,21): error CS1514: { expected 
* Program.cs(4,21): error CS1513: } expected 
* Program.cs(6,5): error CS8803: Top-level statements must precede namespace and type declarations. 
* Program.cs(6,5): error CS0106: The modifier 'public' is not valid for this item 
* Program.cs(24,1): error CS1022: Type or namespace definition, or end-of-file expected 

## Methods

* Methods are called methods when they are inside of a class
* In other languages, you can create 'methods' outside of classes... in that case, we call them functions!
* A method is a reusable block of code... so we don't have to repeat ourselves. DRY.
* A method allows us to modify what a variable or property can do or stores
* A method can be defined with a few additions / modifications:
    * Give the method an access level
    * Give the method a return type (int, string, bool)
    * Give the method parameters

* Program.cs
```csharp
using System;
using Maths;

public class Program
{
    public static void Main()
    {
        
        Mathematics demo = new Mathematics();
        int answer = demo.addition(1,2);
        System.Console.WriteLine(answer);

    }
}
```

* Mathematics.cs
```csharp
using System;

namespace Maths
{
    class Mathematics
    {
        public int addition(int numberOne, int numberTwo)
        {
            return numberOne + numberTwo;
            // addition is storing the value of the sum to be used over and over again
        }
        // give the method a access modifier? Yes. Public.
        // give the method a return type? int
        // give the method parameters? Yes. numberOne, numberTwo
        // why do we put int numberOne, int numberTwo? 
        // C# is a strongly typed language, which means every variable MUST have a type... even if its a parameter
    }
}
```

### Challenge - 40 minutes

1. Create a list:
    * Make it have 5 values to start
    * Add 3 more
    * Use Remove from yesterday
2. Create a stack:
    * Make it have 5 values
    * Add 3 more
    * Delete 2 from the stack
3. Create a queue:
    * Make it have 5 values
    * Add 3 more
    * Delete 2 from the queue
4. Create a Mathematics class (Mathematics.cs)
    * We have addition...
    * Please create subtraction and multiplication
    * Use subtraction and multiplication inside of Program.cs
    
## Inheritance

* Animal.cs
```csharp
using System;

namespace AnimalKingdom
{
    class Animal 
    {
        public string animalName; // letting the user choose the name
        public string location;
        public void noise() 
        {
            Console.WriteLine("Noise");
        }
    }

    class Lion : Animal // lion will take attributes and methods from the Animal class
    {
        public int topSpeed;

        public void hunt()
        {
            Console.WriteLine("I have hunted something.");
        }
    }

    // create an instance of Animal
    // create an instance of Lion
    // compare the two

    class Bird : Animal
    {
        public void fly()
        {
            Console.WriteLine("Yay, I can fly.");
        }
    }

    class Parrot : Bird
    {
        public void chirp()
        {
            Console.WriteLine("chirp, chirp");
        }
    }
}
```

* Program.cs
```csharp
using System;
using AnimalKingdom;

public class Program
{
    public static void Main()
    {

        Animal dave = new Animal();

        Lion leroy = new Lion();

        Bird betty = new Bird();

        Parrot polly = new Parrot();

        // list all of the things that dave can do
        dave.animalName = "Dave";
        dave.noise();
        dave.location = "Manchester";
        // dave.hunt(); // doesn't exist in Animal... only Lion

        // list all of the things that leroy can do
        leroy.animalName = "Leroy";
        leroy.topSpeed = 100;
        leroy.hunt();
        leroy.location = "Loose";

        betty.fly();

        Console.WriteLine("-------");

        polly.noise();
        polly.chirp();
        polly.fly();       
    }
}
```

### Challenge


* Challenge: Please create a MotorVehicle Class with two children / extensions called Car and Motorbike
* MotorVehicle class have 1 variable and 1 method
* Car class have 1 variable and 1 method
* Motorbike class have 1 variable and 1 method

* Create a new folder and in the terminal 
* RUN: 
    dotnet new console 
    dotnet run
* Create a MotorVehicle.cs and go from there!



--- we will come back to the Lion ---
* **Lion**
    * Mane
    * Roar
    * Super strength

# Documentation Links

* [Reserved Keywords](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/)